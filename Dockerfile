# ------------------------------- DEVELOP STAGE -------------------------------
FROM node:18-alpine as develop-stage

# Working directory
WORKDIR /app

# Install dependencies
COPY package.json ./
COPY yarn.lock ./
RUN yarn install

# Copy files
COPY . .

# ------------------------------PRODUCTION STAGE ------------------------------
FROM develop-stage as production-stage

CMD ["npx", "pm2-runtime", "process.json"]
