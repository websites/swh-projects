const path = require("path");
const fastify = require("fastify")({ logger: true });

// variables
const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT || 3000;
const MONGO_URL =
  process.env.MONGO_URL || "mongodb://localhost:27017/app_projects";
const EMAIL_CONFIG = {
  host: process.env.EMAIL_HOST || "sandbox.smtp.mailtrap.io",
  port: parseInt(process.env.EMAIL_PORT) || 2525,
};

// database
fastify.register(require("./plugins/mongoose"), { uri: MONGO_URL });

// email servcer
fastify.register(require("./plugins/nodemailer"), {
  secure: false,
  tls: {
    rejectUnauthorized: false,
  },
  logger: true,
  ...EMAIL_CONFIG,
});

// serve files
fastify.register(require("@fastify/static"), {
  root: path.join(__dirname, "public"),
});

// Parse multipart contents
fastify.register(require("@fastify/multipart"));

// Parse form contents
fastify.register(require("@fastify/formbody"));

// view
fastify.register(require("@fastify/view"), {
  engine: { pug: require("pug") },
  root: path.join(__dirname, "views"),
  includeViewExtension: true,
});

fastify.addHook("preHandler", (request, reply, done) => {
  reply.locals = {
    dayjs: require("dayjs"),
  };
  done();
});

// services
fastify.register(require("./services/projects"));
fastify.register(require("./services/lipn"));

// routes
fastify.register(require("./routes/smallProjects"), { prefix: "/lipn" });
fastify.register(require("./routes/index"));

/**
 * Run the server
 */
const start = async () => {
  try {
    await fastify.listen({ port: PORT, host: HOST });
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
