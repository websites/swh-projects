const { Schema, model } = require("mongoose");
const { projectTypes, users, status } = require("./types");

const smallProjectSchema = new Schema({
  /***************************************************************************
   * PROJECT PRESENTATION
   ***************************************************************************/
  title: {
    type: String,
    required: true,
  },
  summary: {
    type: String,
    required: true,
  },
  keywords: {
    type: [String],
    required: true,
  },
  type: {
    type: String,
    enum: projectTypes,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  /***************************************************************************
   * ENGINEERING REQUIREMENTS
   ***************************************************************************/
  engineer: {
    // days per week
    frequency: {
      type: Number,
      required: true,
    },
    // months
    duration: {
      type: Number,
      required: true,
    },
    tasks: {
      type: String,
      required: true,
    },
  },
  /***************************************************************************
   * WORK ENVIRONMENT
   ***************************************************************************/
  responsible: {
    type: String,
    enum: users,
    required: true,
  },
  /***************************************************************************
   * COMMENTS
   ***************************************************************************/
  comments: {
    type: String,
    default: "",
  },
  management: {
    status: {
      type: String,
      enum: status,
      default: "backlog",
    },
    created_at: {
      type: Date,
      default: new Date(),
    },
    accepted_at: {
      type: Date,
    },
    started_at: {
      type: Date,
    },
    closed_at: {
      type: Date,
    },
  },
});

module.exports = model("SmallProject", smallProjectSchema);
