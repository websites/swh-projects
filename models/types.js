const projectTypes = [
  "proof-of-concept",
  "prototype",
  "maintenance/évolution logiciel existant",
  "prématuration",
];

const laboratories = ["IRIF", "LIPN", "LMF"];

const users = [
  "Etienne ANDRÉ",
  "Jaime ARIAS",
  "Sami EVANGELISTA",
  "Kaïs KLAI",
  "Carlos OLARTE",
  "Laure PETRUCCI",
  "Tayssir TOUILI",
  "Samir YOUCEF",
];

const status = ["backlog", "todo", "doing", "closed"];

module.exports = {
  projectTypes,
  laboratories,
  users,
  status,
};
