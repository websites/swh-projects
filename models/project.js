const { Schema, model } = require("mongoose");
const { projectTypes, laboratories, status } = require("./types");

const projectSchema = new Schema({
  /***************************************************************************
   * PROJECT PRESENTATION
   ***************************************************************************/
  title: {
    type: String,
    required: true,
  },
  summary: {
    type: String,
    required: true,
  },
  keywords: {
    type: [String],
    required: true,
  },
  type: {
    type: String,
    enum: projectTypes,
    required: true,
  },
  expectations: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  /***************************************************************************
   * ENGINEERING REQUIREMENTS
   ***************************************************************************/
  engineer: {
    // days per week
    frequency: {
      type: Number,
      required: true,
    },
    // months
    duration: {
      type: Number,
      required: true,
    },
    skills: {
      type: String,
      required: true,
    },
    tasks: {
      type: String,
      required: true,
    },
  },
  /***************************************************************************
   * WORK ENVIRONMENT
   ***************************************************************************/
  laboratory: {
    type: String,
    enum: laboratories,
    required: true,
  },
  responsible: {
    firstname: {
      type: String,
      required: true,
    },
    lastname: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
  },
  /***************************************************************************
   * COMMENTS
   ***************************************************************************/
  comments: {
    type: String,
    default: "",
  },
  status: {
    type: String,
    enum: status,
    default: "backlog",
  },
});

module.exports = model("Project", projectSchema);
