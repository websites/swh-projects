const fastifyPlugin = require("fastify-plugin");
const { createTransport } = require("nodemailer");

function emailService(fastify, options, next) {
  let transporter = null;

  try {
    transporter = createTransport(options);
  } catch (err) {
    return next(err);
  }

  fastify
    .decorate("nodemailer", transporter)
    .addHook("onClose", (fastify, done) => fastify.nodemailer.close(done));

  next();
}

module.exports = fastifyPlugin(emailService);
