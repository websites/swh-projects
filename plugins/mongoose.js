const fastifyPlugin = require("fastify-plugin");
const mongoose = require("mongoose");

const Project = require("../models/project");
const SmallProject = require("../models/smallProject");
const models = { Project, SmallProject };

async function dbConnector(fastify, options) {
  try {
    mongoose.connection.on("connected", () => {
      fastify.log.info({ actor: "MongoDB" }, "connected");
    });

    mongoose.connection.on("disconnected", () => {
      fastify.log.error({ actor: "MongoDB" }, "disconnected");
    });

    await mongoose.connect(options.uri);

    fastify
      .decorate("db", { models })
      .addHook("onClose", () => mongoose.close());
  } catch (error) {
    fastify.log.error(error);
  }
}

module.exports = fastifyPlugin(dbConnector);
