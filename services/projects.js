const fastifyPlugin = require("fastify-plugin");

const ProjectsService = (model, nodemailer) => {
  const parseBody = (data) => {
    // project presentation
    const { title, summary, keywords, type, expectations } = data;

    // engineer requirements
    const { frequency, duration, skills, tasks } = data;

    // work environment
    const { laboratory, firstname, lastname, email } = data;

    // comments
    const { description, comments } = data;

    // create list of keyword from comma-separated string
    const keywordsList = keywords.split(",").map((k) => k.trim());

    return {
      title,
      summary,
      keywords: keywordsList,
      type,
      expectations,
      description,
      engineer: {
        frequency: parseInt(frequency),
        duration: parseInt(duration),
        skills,
        tasks,
      },
      laboratory,
      responsible: {
        firstname,
        lastname,
        email,
      },
      comments,
    };
  };

  /**
   * Get all the projects
   * @returns Array<Object>
   */
  const getAllProjects = async () => {
    const projects = await model.find({});
    return projects;
  };

  /**
   * Create a new project
   * @returns Project
   */
  const createProject = async (data) => {
    const project = await model.create(parseBody(data));
    const url = `https://love.lipn.univ-paris13.fr/projects/edit/${project._id}`;

    const recipients = {
      IRIF: "direction+projet-ir@irif.fr",
      LIPN: "arias@lipn.univ-paris13.fr",
      LMF: "arias@lipn.univ-paris13.fr",
    };

    // send email
    nodemailer.sendMail({
      from: "love-tools@lipn.univ-paris13.fr",
      to: [recipients[data.laboratory], data.email],
      cc: ["arias@lipn.univ-paris13.fr"],
      subject: "New submission of a software project",
      text: `${data.firstname} ${data.lastname} recently sent a new software development project proposal. You can check it at ${url}`,
    });

    return project;
  };

  /**
   * Get a project by its id
   * @param {String} id identifier of the project
   * @returns Project
   */
  const getProjectById = async (id) => {
    return model.findById(id);
  };

  /**
   * Start a project
   * @param {String} id identifier of the project
   * @returns Project
   */
  const startProject = async (id) => {
    return model.findByIdAndUpdate(id, {
      status: "doing",
    });
  };

  /**
   * Archive a project
   * @param {String} id identifier of the project
   * @returns Project
   */
  const closeProject = async (id) => {
    return model.findByIdAndUpdate(id, {
      status: "closed",
    });
  };

  /**
   * Update a project
   * @param {String} id identifier of the project
   * @param {Object} data new information of the project
   * @returns Project
   */
  const updateProject = async (id, data) => {
    return model.findByIdAndUpdate(id, parseBody(data), { new: true });
  };

  /**
   * Delete a project
   * @param {String} id identifier of the project
   */
  const deleteProject = async (id) => {
    return model.findByIdAndDelete(id);
  };

  /**
   * Write the information of a project into a markdown file
   * @param {Project} project project to serialize
   * @returns path of the file
   */
  const serializeProject = async (project) => {
    const lines = {
      "Cadre de Travail": [
        [
          "Responsable scientifique",
          `${
            project.responsible.firstname
          } ${project.responsible.lastname.toUpperCase()}`,
        ],
      ],
      "Présentation du projet scientifique": [
        ["Nom du project", project.title],
        ["Résumé publiable du projet", project.summary],
        // ["Champs thématiques adressés", project.keywords],
        ["Type", project.type],
        ["Innovations attendues à l'issue du projet", project.expectations],
        ["Argumentaire scientifique qui décrit le projet", project.description],
      ],
      "Description du besoin en ingénierie": [
        ["Frequence", project.engineer.frequency],
        ["Durée", project.engineer.duration],
        [
          "Quelles sont les compétences spécifiques d'ingénierie attendues",
          project.engineer.skills,
        ],
        [
          "Les activités qui seront confiées à·aux ingénieur·e·s",
          project.engineer.tasks,
        ],
      ],
      Commentaires: [["", project.comments]],
    };

    // create the string
    let output = "";
    for (const [title, content] of Object.entries(lines)) {
      output += `# ${title}\n\n`;
      for (let [index, value] of content.entries()) {
        if (index) {
          output += "\n\n";
        }
        const title = value[0];
        if (title.length > 0) {
          output += `## ${title}\n`;
        }

        const text = value[1].toString();
        if (text.length > 0) {
          output += text;
        }
      }
      output += "\n\n";
    }

    return output;
  };

  return {
    getAllProjects,
    getProjectById,
    createProject,
    updateProject,
    deleteProject,
    serializeProject,
    startProject,
    closeProject,
  };
};

module.exports = fastifyPlugin((fastify, options, next) => {
  fastify.decorate(
    "projectsService",
    ProjectsService(fastify.db.models.Project, fastify.nodemailer)
  );
  next();
});
