const fastifyPlugin = require("fastify-plugin");

const SmallProjectsService = (model) => {
  const parseBody = (data) => {
    // project presentation
    const { title, summary, keywords, type, description } = data;

    // engineer requirements
    const { frequency, duration, tasks } = data;

    // work environment
    const { responsible } = data;

    // comments
    const { comments } = data;

    // create list of keyword from comma-separated string
    const keywordsList = keywords.split(",").map((k) => k.trim());

    return {
      title,
      summary,
      keywords: keywordsList,
      type,
      description,
      engineer: {
        frequency: parseInt(frequency),
        duration: parseInt(duration),
        tasks,
      },
      responsible,
      comments,
    };
  };

  /**
   * Get all the projects
   * @returns Array<Object>
   */
  const getAllProjects = async () => {
    const projects = await model.find({});
    return projects;
  };

  const getAcceptedProjects = async () => {
    const projects = await model.find({
      "management.status": { $in: ["todo", "doing"] },
    });

    return projects;
  };

  /**
   * Create a new project
   * @returns Project
   */
  const createProject = async (data) => {
    const project = await model.create(parseBody(data));
    return project;
  };

  const acceptProject = async (id) => {
    return model.findByIdAndUpdate(id, {
      $set: {
        "management.status": "todo",
        "management.accepted_at": new Date(),
      },
    });
  };

  const startProject = async (id) => {
    return model.findByIdAndUpdate(id, {
      $set: {
        "management.status": "doing",
        "management.started_at": new Date(),
      },
    });
  };

  const closeProject = async (id) => {
    return model.findByIdAndUpdate(id, {
      $set: {
        "management.status": "closed",
        "management.started_at": new Date(),
      },
    });
  };

  /**
   * Get a project by its id
   * @param {String} id identifier of the project
   * @returns Project
   */
  const getProjectById = async (id) => {
    return model.findById(id);
  };

  /**
   * Update a project
   * @param {String} id identifier of the project
   * @param {Object} data new information of the project
   * @returns Project
   */
  const updateProject = async (id, data) => {
    return model.findByIdAndUpdate(id, parseBody(data), { new: true });
  };

  /**
   * Delete a project
   * @param {String} id identifier of the project
   */
  const deleteProject = async (id) => {
    return model.findByIdAndDelete(id);
  };

  return {
    getAllProjects,
    getProjectById,
    createProject,
    updateProject,
    deleteProject,
    acceptProject,
    startProject,
    closeProject,
    getAcceptedProjects,
  };
};

module.exports = fastifyPlugin((fastify, options, next) => {
  fastify.decorate(
    "smallProjectsService",
    SmallProjectsService(fastify.db.models.SmallProject)
  );
  next();
});
