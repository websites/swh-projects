const types = require("../models/types");

async function routes(fastify, options) {
  /***************************************************************************
   * PROJECTS
   ***************************************************************************/
  fastify.get("/", async (req, reply) => {
    const projects = await fastify.smallProjectsService.getAllProjects();
    const acceptedProjects =
      await fastify.smallProjectsService.getAcceptedProjects();
    return reply.view("lipn/list", { projects, acceptedProjects });
  });

  fastify.get("/add", async (req, reply) => {
    return reply.view("lipn/add", {
      types: types.projectTypes,
      users: types.users,
      project: { engineer: {} },
    });
  });

  fastify.get("/edit/:id", async (req, reply) => {
    const { id } = req.params;
    const project = await fastify.smallProjectsService.getProjectById(id);

    return reply.view("lipn/edit", {
      types: types.projectTypes,
      users: types.users,
      project: {
        ...project.toObject(),
        keywords: project.keywords.join(", "),
      },
    });
  });

  fastify.post("/", async (req, reply) => {
    await fastify.smallProjectsService.createProject(req.body);
    reply.redirect("/lipn");
  });

  fastify.post("/edit/:id", async (req, reply) => {
    const { id } = req.params;
    await fastify.smallProjectsService.updateProject(id, req.body);
    reply.redirect("/lipn");
  });

  fastify.post("/delete/:id", async (req, reply) => {
    const { id } = req.params;
    await fastify.smallProjectsService.deleteProject(id);
    reply.redirect("/lipn");
  });

  fastify.post("/accept/:id", async (req, reply) => {
    const { id } = req.params;
    await fastify.smallProjectsService.acceptProject(id);
    reply.redirect("/lipn");
  });

  fastify.post("/start/:id", async (req, reply) => {
    const { id } = req.params;
    await fastify.smallProjectsService.startProject(id);
    reply.redirect("/lipn");
  });

  fastify.post("/close/:id", async (req, reply) => {
    const { id } = req.params;
    await fastify.smallProjectsService.closeProject(id);
    reply.redirect("/lipn");
  });
}

module.exports = routes;
