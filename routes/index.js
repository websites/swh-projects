const types = require("../models/types");
const fs = require("fs");
const path = require("path");

async function routes(fastify, options) {
  /***************************************************************************
   * HOME
   ***************************************************************************/
  fastify.get("/", (req, reply) => reply.view("index.pug"));

  /***************************************************************************
   * PROJECTS
   ***************************************************************************/
  fastify.get("/projects", async (req, reply) => {
    const projects = await fastify.projectsService.getAllProjects();
    return reply.view("projects/list", { projects });
  });

  fastify.get("/projects/add", async (req, reply) => {
    return reply.view("projects/add", {
      types: types.projectTypes,
      laboratories: types.laboratories,
      project: { responsible: {}, engineer: {} },
    });
  });

  fastify.get("/projects/edit/:id", async (req, reply) => {
    const { id } = req.params;
    const project = await fastify.projectsService.getProjectById(id);

    return reply.view("projects/edit", {
      types: types.projectTypes,
      laboratories: types.laboratories,
      project: {
        ...project.toObject(),
        keywords: project.keywords.join(", "),
      },
    });
  });

  fastify.post("/projects", async (req, reply) => {
    await fastify.projectsService.createProject(req.body);
    reply.redirect("/projects");
  });

  fastify.post("/projects/edit/:id", async (req, reply) => {
    const { id } = req.params;
    await fastify.projectsService.updateProject(id, req.body);
    reply.redirect("/projects");
  });

  fastify.post("/projects/download/:id", async (req, reply) => {
    const { id } = req.params;
    const project = await fastify.projectsService.getProjectById(id);
    const projectStr = await fastify.projectsService.serializeProject(project);

    const outputFile =
      "./tmp/" +
      project.responsible.lastname.toUpperCase() +
      "-" +
      project.title.toLowerCase().replace(/ /g, "_").replace(/\//g, "_") +
      ".md";

    // create tmp folder
    await fs.promises.mkdir("./tmp", { recursive: true });

    // write content
    await fs.promises.writeFile(outputFile, projectStr, "utf8");

    // send file
    const buffer = fs.readFileSync(outputFile);
    reply.header(
      "Content-Disposition",
      `attachment; filename=${path.basename(outputFile)}`
    );
    reply.send(buffer).type("application/md").code(200);
  });

  fastify.post("/projects/delete/:id", async (req, reply) => {
    const { id } = req.params;
    await fastify.projectsService.deleteProject(id);
    reply.redirect("/projects");
  });

  fastify.post("/projects/start/:id", async (req, reply) => {
    const { id } = req.params;
    console.log("HERE !!");
    await fastify.projectsService.startProject(id);
    reply.redirect("/projects");
  });

  fastify.post("/projects/close/:id", async (req, reply) => {
    const { id } = req.params;
    await fastify.projectsService.closeProject(id);
    reply.redirect("/projects");
  });
}

module.exports = routes;
